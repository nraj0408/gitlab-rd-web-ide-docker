#!/bin/sh

# http://mywiki.wooledge.org/BashFAQ/035
# Usage info
show_help() {
cat << EOF
Usage: ${0##*/} [-h] [-d DOMAIN] [-l LOG_LEVEL]...
Start VS Code Server and Nginx in front of it.

    -h | --help                    display this help and exit
    -d | --domain                  domain
    -l | --log-level               VS Code logging level
    -i | --ignore-version-mismatch ignore vs code client-server version mismatch
EOF
}

die() {
    printf '%s\n' "$1" >&2
    exit 1
}

# Initialize all the option variables.
# This ensures we are not contaminated by variables from the environment.
DOMAIN=
LOG_LEVEL=info
IGNORE_VERSION_MISMATCH=false

while :; do
    case $1 in
        -h|-\?|--help)
            show_help    # Display a usage synopsis.
            exit
            ;;
        -d|--domain)
            if [ "$2" ]; then
                DOMAIN=$2
                shift
            else
                die 'ERROR: "--domain" requires a non-empty option argument.'
            fi
            ;;
        -l|--log-level)
            if [ "$2" ]; then
                case $2 in
                    critical|error|warn|info|debug|trace)
                        LOG_LEVEL=$2
                        ;;
                    *)
                        die 'ERROR: "--log-level" can be only be critical|error|warn|info|debug|trace.'
                        ;;
                esac
                shift
            else
                die 'ERROR: "--log-level" requires a non-empty option argument.'
            fi
            ;;
        -i|--ignore-version-mismatch)
            IGNORE_VERSION_MISMATCH=true
            ;;
        --)              # End of all options.
            shift
            break
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *)               # Default case: No more options, so break out of the loop.
            break
    esac

    shift
done

# Ensure required values are set
if [ -z "$DOMAIN" ]; then
   die 'ERROR: "--domain" is required.'
fi

# Rest of the program here.

cat << EOF

==============================================
Until this notice is removed, this container
should be considered to be in alpha stage
(https://about.gitlab.com/handbook/product/gitlab-the-product/#alpha-beta-ga).
As such it is not encouraged to run it in
production just yet unless you feel capable of
debugging yourself any issue that may arise.
==============================================

EOF

. ./scripts/env.sh

# pre-requisites
mkdir -p log/nginx
touch log/nginx/error.log
touch log/nginx/access.log

# configure and start nginx
cp nginx.conf.template nginx.conf
sed -i "s|ENTER_SERVER_NAME_HERE|${DOMAIN}|g" nginx.conf
nginx -c nginx.conf -p "${ROOT_DIR}"

# generate and save token
TOKEN=$(base64 < /dev/urandom | tr -d 'O0Il1+/' | head -c 44; printf '\n')
echo "$TOKEN" > "${VSCODE_REH_TOKEN_FILE}"

if [ "$IGNORE_VERSION_MISMATCH" = true ]; then
    # remove "commit" key from product.json to avoid client-server mismatch
    echo "Ignoring VS Code client-server version mismatch"
    sed -i '/"commit"/d' "${VSCODE_REH_DIR}/product.json"
fi

# start vs code server
"./${VSCODE_REH_DIR}/bin/code-server-oss" --host "0.0.0.0" --connection-token-file "${VSCODE_REH_TOKEN_FILE}" --log "${LOG_LEVEL}"
